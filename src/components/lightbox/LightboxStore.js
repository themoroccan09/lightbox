class LightboxStore {
  constructor() {
    this.state = {
      images: [],
      current: null
    }
  }

  addImage (url) {
    this.state.images.push(url)
  }

  removeImage (url) {
    this.state.images = this.state.images.filter(i => i !== url)
  }

  next () {
    this.state.current = this.state.images[this.getIndex(this.state.current) + 1];
    if(!this.state.current) {
      this.state.current = this.state.images[0]
    }
  }

  previous () {
    this.state.current = this.state.images[this.getIndex(this.state.current) - 1];
    if(!this.state.current) {
      this.state.current = this.state.images[this.state.images.length - 1]
    }
  }
  close() {
    this.state.current = null
  }

  getIndex (url) {
    return this.state.images.indexOf(url)
  }
}

export default new LightboxStore()