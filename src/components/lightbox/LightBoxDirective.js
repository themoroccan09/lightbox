import Vue from 'vue'
import lightboxStore from './LightboxStore'


Vue.directive('lightbox', {
  bind (el, binding) {
    console.log('bind');
    lightboxStore.addImage(el.getAttribute('href'))
    el.addEventListener('click',(e)=>{
      e.preventDefault()
      lightboxStore.state.current = el.getAttribute('href')
    })
  },
  unbind(el){
    console.log('unbind');
    lightboxStore.removeImage(el.getAttribute('href'))
  }
})
